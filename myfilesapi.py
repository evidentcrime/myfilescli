import argparse, requests
from pathlib import Path

parser = argparse.ArgumentParser()

#auth_token = parser.add_argument("authorizationToken")

auth_token = "50ea2dec-ffe3-4a5e-868b-7c18d04b9e27"

json_body = {"fileName": "https://evidentcrimesamples.s3.amazonaws.com/crime.txt"}

headers = {'authorizationToken': f'{auth_token}'}

args = parser.parse_args()

req = requests.post("https://api.evidentcrime.com/myfiles", headers, json=json_body)

#info: migrated the url to API Gateway which connects to AWS Lambda Function. Please test and let me know.

print(req.content)

#flag{coGvdptNkbKaLWoYFcJGVZTeXmZXWsmqKZTxcdqz}
